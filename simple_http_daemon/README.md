HTTP Daemon for Computer Networking class.

Protocol understands GET requests, and 400, 403, 404, and 200 response codes are implemented.

TODO: Make manual pages, and add more response codes, and see if security is probably horrible.

Run the script with '-h' to see options. 

To run the script executable privileges and run it through your shell. Running through Python 3 interpreter directly will cause errors in argument parsing.

Something like:

```
$ ./httpd.py 1>/dev/null &
```

At least, if you're on a Unix-like.

I have no regrets about the way I wrote the daemon short-term, but I think it would be nice to add more configurable options, and add some security features, make the daemon user-sensitive, add a logger, etc.

All of the code was formatted using the 'black' module.


