#!/usr/bin/env python3

## Z. Wells
## HTTP Daemon that handles GET requests.
## Requires Python 3. Also requires argparse, socket, and datetime modules.

from sys import stderr
import time
from socket import *
import argparse


# Print help file and decide how to handle options
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-p",
        "--port",
        type=int,
        nargs=1,
        required=True,
        help="port for server to listen on",
        dest="port",
    )

    parser.add_argument(
        "-l",
        "--logfile",
        type=str,
        nargs=1,
        default=["logfile"],
        help="Destination for logfile (default 'logfile')",
        dest="logfile",
    )

    return parser.parse_args()


# Open file with error handling
def open_log_file(file):
    try:
        logfile = open(file, "a")
    except PermissionError:
        print("%s: Unable to open file as log: Permission denied", file=stderr)
    except IsADirectoryError:
        print("%s: Unable to open file as log: Is a directory", file=stderr)

    return logfile


# Put requests in to the log
def log_requests(req, nbytes, status, file):
    with open_log_file(file) as logfile:
        host = req[1][1]
        ident = "-"
        authuser = "-"
        date = time.strftime("%d/%b/%Y:%H:%M:%S %z")
        request = " ".join(req[0])
        user_agent = " ".join(req[2][1:])

        print(
            host,
            ident,
            authuser,
            "[" + date + "]",
            '"' + request + '"',
            status,
            nbytes,
            user_agent,
            file=logfile,
        )


# Create the response for the client.
def get_response(filename, protocol):
    responses = []
    code = 0

    try:
        file = open(filename, "r")
    except PermissionError:
        code = 403
        
        file = (
            '\n<html>\n<div class="Header">\n<p>%i Permission Denied</p>\n</div>\n</html>\n'
            % code
        )
        
        responses.insert(0, "%s %i Permission Denied" % (protocol, code))
    except FileNotFoundError:
        code = 404
        
        file = (
            '\n<html>\n<div class="Header">\n<p>%i Not Found</p>\n</div>\n</html>\n'
            % code
        )
        
        responses.insert(0, "%s %i Not Found" % (protocol, code))
    else:
        code = 200
        responses.insert(0, "%s %i OK" % (protocol, code))

    file_contents = "".join(file)
    file_length = len(file_contents) - 1

    # Header fields
    responses.append("Server: Z.W.'s httpd.py")
    responses.append("Date: %s" % time.strftime("%d/%b/%Y:%H:%M:%S %z"))
    responses.append("Content-Length: %s\n" % file_length)

    # file contents
    responses.append(file_contents)

    return {
        "response": "\n".join(responses),
        "log-bytes": file_length,
        "log-status": code,
    }


# Parse the message sent by the client and craft a response
def parse_message(msg, logfilename):
    msg = [line for line in msg.split("\r\n")]
    msg = [line.split(" ") for line in msg]

    response = {}

    response["log-status"] = 400
    
    invalid_request_str = (
        'HTTP/1.1 %i Malformed Request\n\n<html>\n<div class="Header">\n<p>400 Malformed Request</p>\n</div>\n</html>\n\''
        % response["log-status"]
    )
    
    response["log-bytes"] = len(invalid_request_str)

    try:
        request = msg[0][0]
        arg = msg[0][1]
        protocol = msg[0][2]
    except IndexError:
        return invalid_request_str

    if arg == "/":
        arg = "index.html"
    else:
        arg = "." + arg

    if request in "GET":
        response = get_response(arg, protocol)
        log_requests(msg, response["log-bytes"], response["log-status"], logfilename)
        return response["response"]
    else:
        return invalid_request_str


# Set up the sockets
def main():
    args = parse_args()
    server_port = args.port[0]
    logfile = args.logfile[0]

    server_socket = socket(AF_INET, SOCK_STREAM)
    server_socket.bind(("", server_port))
    server_socket.listen(1)

    print("I live to serve.")

    while True:
        try:
            connection_socket, addr = server_socket.accept()
            message = connection_socket.recv(1024)
            modified_message = message.decode()
            response = parse_message(modified_message, logfile)
            connection_socket.send(response.encode())
            connection_socket.close()
        except KeyboardInterrupt:
            print()
            print("Bye")
            server_socket.close()
            break


# Only run main() if script is run directly
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print()
        print("Bye.")

    exit(0)
