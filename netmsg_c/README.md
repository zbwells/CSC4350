## Zander's Networkable Messages

Extremely simple messaging protocol that offers no security and utilizes TCP as transport layer protocol. Written in C with GNU's socket library, and meant for mostly-POSIX-compliant platforms, although that probably doesn't matter in this case.

Written exclusively to see how far my Python network-application development skill transfers over to C, and to see if I even can still write C whatsoever.

### Dependencies:

GNU Sockets library
