/* 
 * Client side of netmsg
 * zbwells
 * 2022
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define MAX_MSG_LEN 2048

void
help(void)
{
    printf("Usage: zcli [OPTION]... [ADDR] [PORT]\n");
    printf("Runs a little server that can receive messages and display them.\n");
    printf("Required arguments:\n");
    printf("  ADDR\t\tthe address the client should connect to\n");
    printf("  PORT\t\tthe TCP port the client should connect to the server on\n");
    printf("\n");
    printf("Options:\n");
    printf("  -h\tDisplay this help message\n");
    printf("  -m <message>\tMessage to send to server\n");
    printf("\n");
}

int 
main(int argc, char **argv)
{
    short option = 0;
    uint16_t port = 0;
    int conn_fd = 0;
    int ch = 0;
    char recv_buffer[MAX_MSG_LEN];
    char send_buffer[MAX_MSG_LEN];
    char *addr;
    char *msg;
    struct sockaddr_in serv_addr;
    
    /* Check to see if the user has asked for help */
    while ((option = getopt(argc, argv, "hm:")) != -1) {
        switch (option) {
            case 'h':
                help();
                return 0;
            case 'm':
            	msg = optarg;
            	break;
            case '?':
                return 1;
        }
    }
    
    if (argc < 3) {
        fprintf(stderr, "zcli: missing required operands\n");
        return 1;
    }
    
    /* Parse the port and addr operands after making sure 
     * there are no bogus arguments */
    for (; optind < argc; optind++) {
        addr = argv[optind];
        port = (uint16_t)atoi(argv[optind+1]);
        
        break;
    }
    
    /* Properly zero out structures */
    memset(&serv_addr, '\0', sizeof(serv_addr));
    memset(recv_buffer, '\0', sizeof(recv_buffer));
    memset(send_buffer, '\0', sizeof(recv_buffer));
    
    /* Attempt to create socket */
    if ((conn_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        fprintf(stderr, "Error: could not create socket\n");
        return 1;
    }
    
    /* Configure server network struct */
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    serv_addr.sin_addr.s_addr = inet_addr(addr);
    
    printf("client is attempting to connect to %s:%u\n", addr, port);
    
    /* Connect socket to address/port */
    if (connect(conn_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        fprintf(stderr, "Error: failed to connect to server\n");
        return 1;
    }
    
    strncpy(send_buffer, 
            msg, 
            MAX_MSG_LEN-1);
    
    if (send_buffer[sizeof(send_buffer)-1] != '\n') {
    	strncat(send_buffer, "\n", sizeof(send_buffer)-1);
    }
            
    write(conn_fd, send_buffer, strlen(send_buffer));
    
    read(conn_fd, recv_buffer, sizeof(recv_buffer));
    printf("%s\n", recv_buffer);
	
	close(conn_fd);
	fflush(stdout);
    return 0;
}
