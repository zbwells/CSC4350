/* 
 * Server side of netmsg
 * zbwells
 * 2022
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define MAX_MSG_LEN 2048

void
help(void)
{
    printf("Usage: zserv [OPTION]... [PORT]\n");
    printf("Runs a little server that can receive messages and display them.\n");
    printf("PORT is a required argument ");
    printf("which corresponds with the TCP port the server should listen on.\n");
    printf("\n");
    printf("Options:\n");
    printf("  -h, --help\tDisplay this help message\n");
    printf("\n");
}

int 
main(int argc, char **argv)
{
    short option = 0;
    uint16_t port = 0;
    int listen_fd = 0;
    int conn_fd = 0;
    int ch = 0;
    struct sockaddr_in serv_addr;
    char recv_buffer[MAX_MSG_LEN];
    char send_buffer[MAX_MSG_LEN];
    
    if (argc < 2) {
        fprintf(stderr, "zserv: missing port operand\n");
        return 1;
    }
    
    /* Check to see if the user has asked for help */
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch (option) {
            case 'h':
                help();
                return 0;
            case '?':
                return 1;
        }
    }
    
    /* Parse the port operand after making sure there are no bogus arguments */
    for (; optind < argc; optind++) {
        port = (uint16_t)atoi(argv[optind]);
    }
    
    /* Create the socket for listening */
    if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        fprintf(stderr, "Error: could not create socket\n");
        return 1;
    }
    
    /* Properly zero out data in structures */
    memset(&serv_addr, '\0', sizeof(serv_addr));
    memset(recv_buffer, '\0', sizeof(recv_buffer));
    memset(send_buffer, '\0', sizeof(send_buffer));
    
    /* Configure server network struct */
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    /* Bind socket to address/port */
    if (bind(listen_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        fprintf(stderr, "Error while binding to socket; is port in use already?\n");
        return 1;
    }
    
    /* Start listening on port */
    if (listen(listen_fd, 10) < 0) {
        fprintf(stderr, "Error: failed to listen on port\n");
    }
    
    printf("now listening on port %u\n", port);
    
    for (;;) {
        conn_fd = accept(listen_fd, (struct sockaddr*)NULL, NULL);
        read(conn_fd, recv_buffer, sizeof(recv_buffer)-1);
		printf("%s", recv_buffer);
		
		strncpy(send_buffer, 
            "success", 
            MAX_MSG_LEN-1);
        
		write(conn_fd, send_buffer, 19);       
        close(conn_fd);
        fflush(stdout);
    }
    
    return 0;
}
