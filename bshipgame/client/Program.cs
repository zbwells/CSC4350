using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

/* Prototype client for Networked Battleship game.
 * doesn't do much, should be replaced by graphical version one day.
 * zbwells and crew
 */

class Client
{
    /* Setup func threaded to make waiting for networking not stop other operations */
    public static async Task Main(string[] args)
    {
        string addrstr = "";
        int port = 0;

        /* If help requested or no args, print help line */
        if (args.Length < 2 || args[0] == "-h" || args[0] == "--help")
        {
            Console.WriteLine("Usage: bship_client <addr> <port>");
            return;
        }
        else
        {
            addrstr = args[0];
            port = int.Parse(args[1]);
        }

        /* Parse ipaddress and send it on its way */
        IPAddress ipaddress = IPAddress.Parse(addrstr);
        await Request(ipaddress, port);
    }

    public static async Task Request(IPAddress ipaddr, int port)
    {   
        var ipEndPoint = new IPEndPoint(ipaddr, port);

        /* Get network stream from connection after attempting to connect */
        using TcpClient client = new();
        await client.ConnectAsync(ipEndPoint);
        await using NetworkStream stream = client.GetStream();
        
        for (;;)
        {        
            /* Receive stuff from server */
            var buffer = new byte[100];
            int received = await stream.ReadAsync(buffer);
            var message = Encoding.UTF8.GetString(buffer, 0, received);   
            Console.Write($"{message}");

            /* Write stuff to the server */            
            if (message is null || message[0] == '!')
            {
                continue;
            }
            else if (message[0] == '#')
            {
                string sendmsg = Console.ReadLine() ?? String.Empty;
                await stream.WriteAsync(Encoding.ASCII.GetBytes(sendmsg));
            }

            /* Flush network stream so it can be re-used */
            stream.Flush(); 
        }
    }
}
