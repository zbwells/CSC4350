Server and Client for Computer Networking class.

Protocol understands IP, PORT, and TIMEDELAY methods. Will return OK or INVALID depending on whether or not the request is understood by the server.

TODO: Make manual pages. Currently, to see usage for the client and server, just use '-h' or '--help' to view options and arguments.

Used the Python socket library; for the server, mostly seperated the jobs of the UDP and TCP server apart, but on the client handled them together in close proximity, simply because it wouldn't harm the readability all that much. The interface responds to both menu numbers and whole words, and also itself has a help command, although it's somewhat redundant.

To run, either use Python 3 directly or give both scripts executable privileges and run them that way.

Something like:

```
$ ./server.py 1>/dev/null &
$ ./client.py
```

At least, if you're on a Unix-like.

The server does not have to run before the client, as the client does not care whether or not there is a server on the other end-- although it will tell you if there is not one.

More time was spent on the client than the server, so the server contains more syntactic redundancy than the client does. Efforts have been made to cut down on superfluous comments to increase readability, although this may sacrifice some understanding if the reader is somewhat unfamiliar with socket programming.

All of the code was formatted using the 'black' module.

