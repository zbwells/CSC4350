#!/usr/bin/env python3

## Z. Wells
## Server application built to send either TCP or UDP requests.
## Requires Python 3. Also requires argparse, socket, and datetime modules.

from sys import stderr
from datetime import datetime
from socket import *
import argparse

# Print help file and decide how to handle options
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-p",
        "--port",
        type=int,
        nargs=1,
        default=[7777],
        help="port for server to listen on (default 7777)",
        dest="port",
    )
    
    parser.add_argument(
        "-t",
        "--type",
        type=str,
        nargs=1,
        default=["tcp"],
        help="type of connection (tcp/udp)",
        dest="type",
    )
    
    parser.add_argument(
        "-l",
        "--logfile",
        type=str,
        nargs=1,
        default=["logfile"],
        help="Destination for logfile (default 'logfile')",
        dest="logfile",
    )

    return parser.parse_args()


# Open file with error handling
def open_log_file(file):
    try:
        logfile = open(file, "a")
    except PermissionError:
        print("%s: Unable to open file as log: Permission denied", file=stderr)
    except IsADirectoryError:
        print("%s: Unable to open file as log: Is a directory", file=stderr)
    
    return logfile
    

# Put requests in to the log
def log_requests(msg, resp, file):
    with open_log_file(file) as logfile:
        print(datetime.now(), msg, resp[:resp.find('\n')], file=logfile)
        

# Parse the message sent by the client and craft a response
def parse_message(addr, msg, args):
    if msg[:2] == "IP":
        response = str(addr[0])
    elif msg[:4] == "PORT":
        response = str(addr[1])
    elif msg[:9] == "TIMEDELAY":
        response = str(datetime.now())
    else:
        return "INVALID\n"
    
    return "OK" + "\n" + response
    
    
# Set up the sockets and decide which protocol to use.
def main():
    args = parse_args()
    server_port = args.port[0]
    conn_type = args.type[0]
    logfile = args.logfile[0]
    
    def serve_tcp():
        server_socket = socket(AF_INET, SOCK_STREAM)
        server_socket.bind(("", server_port))
        server_socket.listen(1)
        
        while True:
            try:
                connection_socket, addr = server_socket.accept()
                message = connection_socket.recv(1024)
                modified_message = message.decode().upper()
                response = parse_message(addr, modified_message, args)
                
                connection_socket.send(response.encode())
                log_requests(modified_message, response, logfile)
                connection_socket.close()
            except KeyboardInterrupt:
                print()
                print("Bye")
                server_socket.close()
                break   
                 
    def serve_udp():
        server_socket = socket(AF_INET, SOCK_DGRAM)
        server_socket.bind(("", server_port))
        
        while True:
            try:
                message, client_address = server_socket.recvfrom(2048)
                modified_message = message.decode().upper()
                response = parse_message(client_address, modified_message, args)
                server_socket.sendto(response.encode(), client_address)
                log_requests(modified_message, response, logfile)
             
                
            except KeyboardInterrupt:
                print()
                print("Bye")
                server_socket.close()
                break        
      
    print("I live to serve.")
    
    if conn_type == "tcp":
        serve_tcp()
    elif conn_type == "udp":
        serve_udp()
    else:
        print(conn_type, "not supported, using tcp")
        serve_tcp()


# Only run main() if script is run directly
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print()
        print("Bye.")
        
    exit(0)
