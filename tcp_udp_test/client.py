#!/usr/bin/env python3

## Z. Wells
## Client application built to send either TCP or UDP requests.
## Requires Python 3, the datetime, and socket modules.

from datetime import datetime
from sys import stderr
from socket import *
import argparse


# Handles command line arguments
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-p",
        "--port",
        type=int,
        nargs=1,
        default=[7777],
        help="port for client to connect to (default 7777)",
        dest="port",
    )

    parser.add_argument(
        "-t",
        "--type",
        type=str,
        nargs=1,
        default=["tcp"],
        help="type of connection (tcp/udp)",
        dest="type",
    )
    
    parser.add_argument(
        "-i",
        "--ip",
        type=str,
        nargs=1,
        default=["127.0.0.1"],
        help="address to connect to (default 127.0.0.1)",
        dest="addr",
    )

    return parser.parse_args()


# Prints the in-interface help message
def print_help():
    print("requests:")
    print("1. IP - returns client's ip address")
    print("2. PORT - returns port client has bound to")
    print("3. TIMEDELAY - returns datetime of server and delay between server and client")
    print()
    print("controls:")
    print("4. QUIT - exits client interface")
    print("5. HELP - prints this list again")
    print()


def gone():
    print()
    print("Bye.")
    exit(0)


# Parses responses after they've been received but before outputting anything to screen
# Responses are always preceded by response codes "OK" or "INVALID" followed by a newline
# Check TIMEDELAY before OK to reduce artificial delay introduced by client-side calculations.
def parse_post_response(msg, resp):
        resp = resp.decode()
        resp_data = resp[resp.find('\n')+1:]
        resp_code = resp[:resp.find('\n')]
        
        if resp_code == "INVALID":
            print("Invalid Response")     
        elif msg.upper()[:11] == "TIMEDELAY":
            # voodoo magic possible because datetime.now() always returns a fixed length string
            clienttime = float(str(datetime.now())[-9:])
            servertime = float(resp_data[-9:])
            print(resp_data)      
            print("%.2fms" % ((clienttime-servertime)*1000))
        elif resp_code == "OK":    
            print(resp_data)

    
# Exits process or returns 1 if server should be sent something; returns 2 for exit; otherwise, returns 0
def interface_control(msg):
    if msg.upper()[:1] == "Q":
        return 2
    elif msg.upper()[:1] == "H":
        print_help()
        return 1
    else:
        return 0


# Convert menu numbers into actual commands
def get_request_from_menu(msg):
    if msg == '1':
        return "IP"
    elif msg == '2':
        return "PORT"
    elif msg == '3':
        return "TIMEDELAY"
    elif msg == '4':
        return "QUIT"
    elif msg == '5':
        return "HELP"
    else:
        return msg

        
# Attempts to connect to the server
def connect(info, help_printed):
    protocol_is_tcp = lambda : info["conn_type"] == "tcp"
    protocol_is_udp = lambda : not protocol_is_tcp()
    
    # This variable is an array so that it can be passed by reference
    if help_printed[0] == 0:
        print_help()
        help_printed[0] = 1

    message = get_request_from_menu(input("> "))    
    control = interface_control(message)
    
    if message == "":
        return
    
    if control == 2:
        gone()
    if control == 1:
        return
        
    if protocol_is_tcp():
        clisock = socket(AF_INET, SOCK_STREAM)
        try:
            clisock.connect((info["server_name"], info["server_port"]))
        except ConnectionRefusedError:
            print("Connection Refused at %s:%s" % (info["server_name"], info["server_port"]), file=stderr)
            return
        
        clisock.send(message.encode())
            
    elif protocol_is_udp():
        clisock = socket(AF_INET, SOCK_DGRAM)
        clisock.settimeout(5)
        clisock.sendto(message.encode(), (info["server_name"], info["server_port"]))
    
    try:
        response, server_address = clisock.recvfrom(2048)
    except:
        print("Server not responding at %s:%s" % (info["server_name"], info["server_port"]), file=stderr)
        return    

    parse_post_response(message, response)      
       
            
def main():
    args = parse_args()
    
    serv_info = { 
        "server_name"  : args.addr[0], 
        "server_port"  : args.port[0], 
        "conn_type"    : args.type[0],  
    }
    
    help_printed = [0]
    
    if serv_info["conn_type"] != "udp" and serv_info["conn_type"] != "tcp":
         print(serv_info["conn_type"], "not supported, using tcp instead", file=stderr)
         serv_info["conn_type"] = "tcp"

    while True:
        try:
            connect(serv_info, help_printed)
        except (KeyboardInterrupt, EOFError):
            gone()
    

# Only run main() if script is run directly
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        gone()
        
    exit(0)
