# Literally The Worst Protocol in The World
### is actually pretty cool, but handicaps itself to prove it can stand up to the DDosers

Simple UDP data transfer client and server for Computer Networking class.
Has basic principles of reliable transfer built-in, and theoretically could be used to transfer data safely even in an unstable network.

When sent data, once it safely receives it all, it will simply regurgitate the data on the other end. I'm starting to see how I could easily turn this into a one-sided file transfer protocol.

To execute the programs, set the executable bit on the scripts, and then do something like this:

```
$ ./server.py 1>/dev/null &
$ ./client.py
```

Use `--help` to view command line options.

The code works perfectly fine from an outsider's perspective, but I haven't the time as of Nov 9 2022 to do extensive testing on either client or server side, and the assignment is due at the end of the week, and so it will stay this way until then, because I have other things to work on and this works pretty well.
Additionally, there are some obvious things that just don't work the way the output would suggest they do: the client and server are doing completely different checksum calculations, the source for which is located in header.py, but is unintuitive to read. This can be easily fixed in theory, but in practice requires refactoring a lot of things.
If you are the person reviewing my code, please try to figure out why the corrupt_packet() function in client.py occasionally corrupts the protocol identifier byte at the beginning of the payload.

Thanks!
