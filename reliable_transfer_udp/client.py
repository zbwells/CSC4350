#!/usr/bin/env python3

## Z. Wells
## Client application built to send to a UDP port, but with reliable data transfer principles and stuff
## Python 3.x required, argparse required, socket library required
## Execute using a shell and set the executable flag on the file

#### POSSIBLE FLAGS ####
# SND : 00000001 : 0x01
# ACK : 00000010 : 0x02
# NACK: 00000100 : 0x04
########################

### POSSIBLE SEQUENCE NUMBERS ####
# RIGHT: 00001111 0x0F
# LEFT : 11110000 0xF0
##################################

from random import randint
from sys import stderr
from socket import *
import argparse
from header import *


# Handles command line arguments
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i",
        "--ip",
        type=str,
        nargs=1,
        default=["127.0.0.1"],
        help="address of the server (default loopback)",
        dest="ip",
    )

    parser.add_argument(
        "-p",
        "--port",
        type=int,
        nargs=1,
        default=[7777],
        help="port of the UDP listener on the server (default 7777)",
        dest="port",
    )

    parser.add_argument(
        "-m",
        "--message",
        type=str,
        nargs=1,
        default="test",
        help="message to be sent to the server (default 'test')",
        dest="msg",
    )

    return parser.parse_args()


def gone():
    print()
    print("Bye.")
    exit(0)


# Split the message into 8 byte segments, and make valid headers for each one
# Flags will always be 0x01, SND, because this is the client
def msg_mkproper(msg, servport):
    bytecount = 0
    segments = []
    tmpseg = bytes()
    tmpheader = bytes()
    seq_num = Sequence.RIGHT
    last_seq = bytes()

    # sanitize input
    msg = "".join(msg)

    for ch in msg:
        tmpseg += ch.encode()
        bytecount += 1

        if bytecount % 8 == 0 or bytecount == len(msg):
            while len(tmpseg) < 8:
                tmpseg += b"\x00"

            seq_num = (seq_num == Sequence.RIGHT and Sequence.LEFT) or Sequence.RIGHT
            tmpheader = msg_mkheader(tmpseg, servport, Flags.SND, seq_num)
            segments.append(tmpheader + tmpseg)
            tmpseg = bytes()

    return segments


# Corrupt packet with \x90, but exclude the identifier.
def corrupt_packet(payload):
    new_payload = bytes()
    counter = 0
    target = randint(1, len(payload) - 1)

    new_byte = bytes()

    for byte in payload:
        if counter == target:
            new_byte = b"\x90"
        else:
            new_byte = bytes([byte])

        new_payload += new_byte
        counter += 1

    return new_payload


def main():
    args = parse_args()
    
    # Magic constants
    flag_field = 6
    end_header = 10
    
    info = {
        "server_name": args.ip[0],
        "server_port": args.port[0],
        "message": args.msg,
    }

    if isinstance(info["message"], str):
        info["message"] += "\n"
    else:
        info["message"].append("\n")

    payload = msg_mkproper(info["message"], info["server_port"])
    clisock = socket(AF_INET, SOCK_DGRAM)
    clisock.settimeout(2)

    timeouts = 0
    packetindexlist = list(range(len(payload)))
    
    for i in packetindexlist:
        packet = payload[i]
        
        try:
            real_packet = packet[::]
            if randint(0, end_header) == 1:
                packet = corrupt_packet(packet)

            clisock.sendto(packet, (info["server_name"], info["server_port"]))
            response, server_address = clisock.recvfrom(2048)

            # While flag is NACK
            while response[flag_field] == ord(Flags.NACK):
                clisock.sendto(real_packet, (info["server_name"], info["server_port"]))
                response, server_address = clisock.recvfrom(2048)

        except (KeyboardInterrupt, EOFError):
            gone()
        except timeout:
            if timeouts < 5:
                packetindexlist.insert(i+1, i)
                timeouts += 1
            else:    
                print(
                    "Server not responding at %s:%s"
                    % (info["server_name"], info["server_port"]),
                    file=stderr,
                )
                gone()


# Only run main() if script is run directly
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        gone()

    exit(0)
