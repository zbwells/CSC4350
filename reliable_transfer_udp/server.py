#!/usr/bin/env python3

## Z. Wells
## Server built to listen on a UDP port, but with reliable transfer principles builtin.
## Python 3.x required, argparse required, socket library required
## Execute using a shell and set the executable flag on the file

#### POSSIBLE FLAGS ####
# SND : 00000001 : 0x01
# ACK : 00000010 : 0x02
# NACK: 00000100 : 0x04
########################

### POSSIBLE SEQUENCE NUMBERS ####
# RIGHT: 00001111 0x0F
# LEFT : 11110000 0xF0
##################################

from sys import stdout
from sys import stderr
from socket import *
from random import randint
import argparse
from header import *

# Print help file and decide how to handle options
def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-p",
        "--port",
        type=int,
        nargs=1,
        default=[7777],
        help="port for server to listen on (default 7777)",
        dest="port",
    )

    return parser.parse_args()

# Decide whether or not to drop a packet
def simulate_packet_loss():
    return randint(0, 10) == 5
    

# Set up the sockets
def main():
     
    args = parse_args()
    server_port = args.port[0]
    
    # Magic constants
    chksum_begin = 8
    end_header = 10
    sequence_field = 7
    
    server_socket = socket(AF_INET, SOCK_DGRAM)
    server_socket.bind(("", server_port))

    print("I live to serve.")
    
    lastpacket = b''
    
    while True:
        lost = bool()
        flag = Flags.ACK
        try:
            message, client_address = server_socket.recvfrom(2048)
            
            # If duplicate packets, do not display
            if message == lastpacket:
                lost = True
            
            # Randomly fail to process everything
            if simulate_packet_loss():
                continue
            
            if message[0] != ord("A"):
                print("Invalid protocol", file=stderr)
                break

            if message[chksum_begin:end_header] != server_side_checksum(message):
                flag = Flags.NACK
            
            # Randomly fail respond, but still process properly
            if not simulate_packet_loss():
                response = msg_mkheader(
                    message, client_address[1], flag, bytes([message[sequence_field]])
                )
                
                server_socket.sendto(response, client_address)
                
                
            if flag == Flags.NACK:
                continue
            
            if not lost:
                print("".join(list(map(chr, (message[end_header:])))), end="")
            
            stdout.flush()
            lastpacket = message

        except KeyboardInterrupt:
            print()
            print("Bye")
            server_socket.close()
            break


# Only run main() if script is run directly
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print()
        print("Bye.")

    exit(0)
