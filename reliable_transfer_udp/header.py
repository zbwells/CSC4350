from sys import stderr
from random import randint
import enum

# Module with some header related stuff in it

### Magic Numbers ###
class Flags:
    SND = b'\x01'
    ACK = b'\x02'
    NACK = b'\x04'

class Sequence:
    RIGHT = b'\x0F'
    LEFT = b'\xF0'
    

## Checksum Calculation ##
def server_side_checksum(msg):
    chksum_low = 0
    chksum_high = 0
    
    # 8 and 9 are the positions of the previous checksum
    for i in range(0, len(msg), 2):
        if i == 8 or i == 9:
            continue
            
        chksum_low ^= msg[i]
        chksum_high ^= msg[i+1]
    
    chksum = bytes((chksum_low, chksum_high))

    return chksum

def client_side_checksum(msg, headervalues):
    chksum_low = 0
    chksum_high = 0
    
    flattened_header = [val for sub in headervalues for val in sub]
    for i in range(0, len(flattened_header), 2):
        chksum_low ^= flattened_header[i]
        chksum_high ^= flattened_header[i+1]

    for i in range(0, len(msg), 2):
        chksum_low ^= msg[i]
        chksum_high ^= msg[i+1]
    
    chksum = bytes((chksum_low, chksum_high))
        
    return chksum


# Create the header and make sure it has all of the correct properties
def msg_mkheader(msg_seg, dport, flags, seq_num):
    header_size = 10
    header = dict()

    msg_length = len(msg_seg)

    serv_flags = lambda: flags == b"\x02" or flags == b"\x04"

    if serv_flags():
        msg_length = 0

    header["protocol_identifier"] = b"\x41"
    header["length"] = bytes([header_size + msg_length])

    # Use divmod to split the number into two bytes
    header["destination_port"] = bytes(divmod(dport, 256))
    header["random_number"] = bytes(divmod(randint(0x0000, 0xFFFF), 256))

    # These depend on external factors
    header["flags"] = flags
    header["sequence_number"] = seq_num

    chksum = 0

    if serv_flags():
        header["checksum"] = server_side_checksum(msg_seg)
    else:
        header["checksum"] = client_side_checksum(msg_seg, list(header.values()))

    # Make a bytes object with all the header
    header_obj = bytes()
    for v in header.values():
        header_obj += bytes(v)

    # Assert that header is of size 10
    if len(header_obj) != 10:
        print("ERROR: Header sent with incorrect size", file=stderr)

    return header_obj
